﻿using NUnit.Framework;
using UI_Testing_30._09._21.Support;

namespace UI_Testing_30._09._21.Support
{
    public class Hooks
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://newbookmodels.com/");
        }
        //[SetUp]
        //public void SetUp()
        //{
        //    ChromeBrowser.GetDriver().Navigate().GoToUrl("https://newbookmodels.com/");
        //}

        [TearDown]
        public void TearDown()
        {
            ChromeBrowser.CloseDriver();
            //ChromeBrowser.CleanDriver();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            //ChromeBrowser.CleanDriver();
            ChromeBrowser.CloseDriver();
        }
    }
}
