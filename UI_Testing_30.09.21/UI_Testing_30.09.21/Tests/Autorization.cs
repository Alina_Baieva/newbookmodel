﻿using UI_Testing_30._09._21.POM.Methods;
using UI_Testing_30._09._21.Support;

namespace UI_Testing_30._09._21.Tests_Methods
{
    class Autorization : Hooks
    {
        public void LogIn (string name, string password)
        {
            AuthMethods.ClickButtonLogIn();
            AuthMethods.SetAuthField(name,password);
            AuthMethods.ClickLogIn();
        }
    }
}