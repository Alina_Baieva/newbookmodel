﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;
using UI_Testing_30._09._21.Support;
using UI_Testing_30._09._21.POM.Methods;
using NUnit.Framework;

namespace UI_Testing_30._09._21
{
   public class Registration : Hooks
    {
        [Theory]
        [TestCase("Алина", "Linaluna", "gew666713@bpkj.com", "Qwerty123!", "Qwerty123!", "1234560789", "lino4kalunafur", "luna.com", "Usa", "https://newbookmodels.com/join/company")]
        [TestCase("L", "1", "m@gmail.com", "Qwerty1!", "Qwerty1!", "1111112222", "2", "M.com", "Usa", "https://newbookmodels.com/join/company")]
        [TestCase("BBBaaaahjdfco11111111111cdsfvdsfvdsvds212021546ddd", "BBBaaaahjdfcogh11222c25fvdsfvdsvds212021546dddmmmm", "mmmm2222222222222333vgkgh222222222222nnn@gmail.com", "Qwerty123?125nn22iAAAAn0!", "Qwerty123?125nn22iAAAAn0!", "9999999999", "lilalalal049032324rfeagaerg34r12fdrgfghdhtaggrf23r", "lilalalal049032324rfeagae222rg34r12fdrgfghdhta.com", "Usa", "https://newbookmodels.com/join/company")]
        public void ValidTest(string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber, string companyname, string compwebsite, string adress, string expectedUrl)
        {
            RegistrationPage1Methods.GetButtonSignUp().Click();
            RegistrationPage1Methods.Registration(firstname, lastname, GenerateNewMail.GenerateMail(), password, confirmpassword, mobNumber);
            RegistrationPage1Methods.GetButtonNext().Click();

            RegistrationPage2Methods.Registration(companyname, compwebsite, adress);
            RegistrationPage2Methods.SelectIndustry();
            RegistrationPage2Methods.GetButtonFinish().Click();

            var actualUrl = RegistrationPage2Methods.GetUrl();
            Assert.AreEqual(expectedUrl,actualUrl);
        }
        //[Theory]
        //[TestCase("", "Linaluna", "gew666713@bpkj.com", "Qwerty123!", "Qwerty123!", "1234560789", "Required")]
        //[TestCase("Lina", "", "gew666mm3@bpkj.com", "Qwerty123!", "Qwerty123!", "1234560789", "Required")]
        //[TestCase("Lina", "Linaluna", "", "Qwerty123!", "Qwerty123!", "1234560789", "Required")]
        //[TestCase("Лина", "luna", "aasheaeh3@kj.com", "", "Qwerty123!", "1234560789", "Invalid password format")]
        //[TestCase("Li", "Linalu", "g3@bpmmj.com", "Qwerty123!", "", "1234560789", "Passwords must match")]
        //[TestCase("Lim", "Linalu", "mm3@bp.com", "Qwerty123!", "Qwerty123!", "", "Invalid phone format")]
        //public void InValidTest(string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber, string companyname, string compwebsite, string adress, string expectedUrl)
        //{
        //    RegistrationPage1Methods.GetButtonSignUp().Click();
        //    RegistrationPage1Methods.Registration(firstname, lastname, GenerateNewMail.GenerateMail(), password, confirmpassword, mobNumber);
        //    RegistrationPage1Methods.GetButtonNext().Click();

        //    RegistrationPage2Methods.Registration(companyname, compwebsite, adress);
        //    RegistrationPage2Methods.SelectIndustry();
        //    RegistrationPage2Methods.GetButtonFinish().Click();

        //    var actualUrl = RegistrationPage2Methods.GetUrl();
        //    Assert.AreEqual(expectedUrl, actualUrl);
           
        }
}
