﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_Testing_30._09._21
{
    class GenerateNewMail
    {
        const string name0 = "test";
        const string name1 = "Luna";
        const string name2 = "lLove";

        const string name3 = "JKlddk";
        const string name4 = "merryd";
        const string name5 = "fghte";
        public static string GenerateMail()
        {
            string randomEmail = "";
            Random random = new Random();
            var name = random.Next(0, 3);
            switch (name)
            {
                case 0:
                    randomEmail += name0;
                    break;
                case 1:
                    randomEmail += name1;
                    break;
                case 2:
                    randomEmail += name2;
                    break;
            }
            randomEmail += (DateTime.Now.ToBinary() * -1).ToString() + "@gmail.com";
            return randomEmail;
        }
        public static string GenerateMailWithSpecialCharacters()
        {
            string randomEmail = "";
            Random random = new Random();
            var name = random.Next(0, 3);
            switch (name)
            {
                case 0:
                    randomEmail += name3;
                    break;
                case 1:
                    randomEmail += name4;
                    break;
                case 2:
                    randomEmail += name5;
                    break;
            }
            var specialChar = "!#$%&‘*+—/=?^_`{|}~,";
            var charNumber = random.Next(0, 21);
            for (int i = 0; i < specialChar.Length; i++)
            {
                if (charNumber == i)
                {
                    randomEmail += specialChar[i];
                    break;
                }
            }
            randomEmail += (DateTime.Now.ToBinary() * -1).ToString() + "@gmail.com";
            return randomEmail;
        }
    }
}
