﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using UI_Testing_30._09._21.POM.Locators;
using UI_Testing_30._09._21.Support;

namespace UI_Testing_30._09._21.POM.Methods
{
    internal class RegistrationPage2Methods
    {
        static IWebElement GetCompanyName() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._companyName);
        static IWebElement GetCompanyWebSite() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._companySite);
        static IWebElement GetCompanyLocation() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._location);
        static IWebElement GetMapsLocation() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._listLocation);
        static IWebElement GetIndustry() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._selectIndustry);
        static IWebElement GetCosmaticsIndustry() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._cosmaticsIndustry);
        static IWebElement GetOtherIndustry() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._otherIndustry);
        static  IWebElement GetInputIndustry() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._inputIndustry);
        public static IWebElement GetButtonFinish() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._buttonFinish);
        public static string GetUrl() => ChromeBrowser.GetDriver().Url;
        public static string GetErrorCompanyName() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._companyNameError).Text;
        public static string GetErrorCompanySiteError() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._companySiteError).Text;
        public static string GetErrorAdress() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._adressError).Text;
        public static string GetErrorIndustry() => ChromeBrowser.GetDriver().FindElement(RegistrationPage2Locators._industryError).Text;
        public static void Registration(string companyName, string companySite, string adress)
        {
            var FieldCompanyName = GetCompanyName();
            FieldCompanyName.Click();
            FieldCompanyName.SendKeys(companyName);
            FieldCompanyName.SendKeys(Keys.Enter);

            var FieldWebSite = GetCompanyWebSite();
            FieldWebSite.Click();
            FieldWebSite.SendKeys(companySite);
            FieldWebSite.SendKeys(Keys.Enter);

            var FieldLocation = GetCompanyLocation();
            FieldLocation.Click();
            FieldLocation.SendKeys(adress);
            FieldLocation.SendKeys(Keys.Up);
            Actions action = new Actions(ChromeBrowser.GetDriver());
            action.MoveToElement(GetMapsLocation());
            action.KeyDown(Keys.LeftShift);

        }
        public static void SelectIndustry()
        {
            GetIndustry().Click();
            GetCosmaticsIndustry().Click();
        }
        public static void OtherIndustry(string nameIndustry)
        {
            GetIndustry().Click();
            GetOtherIndustry().Click();
            var fieldIndustry = GetInputIndustry();
            fieldIndustry.SendKeys(nameIndustry);
            fieldIndustry.SendKeys(Keys.Enter);
        }
    }
}
