﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI_Testing_30._09._21.POM.Locators;
using UI_Testing_30._09._21.Support;

namespace UI_Testing_30._09._21.POM.Methods
{
    internal class AuthMethods
    {
        private static IWebElement GetLogin() => ChromeBrowser.GetDriver().FindElement(AuthLocators._login);
        private static IWebElement GetPassword() => ChromeBrowser.GetDriver().FindElement(AuthLocators._password);
        internal static void ClickButtonLogIn()=> ChromeBrowser.GetDriver().FindElement(AuthLocators._LoginButton).Click();
        internal static void ClickLogIn() => ChromeBrowser.GetDriver().FindElement(AuthLocators._buttonLogIn).Click();
        internal static string GetUrl() => ChromeBrowser.GetDriver().Url;
        internal static string GetErrorLogin() => ChromeBrowser.GetDriver().FindElement(AuthLocators._loginError).Text;
        internal static string GetErrorPassword() => ChromeBrowser.GetDriver().FindElement(AuthLocators._passwordError).Text;

        public static void SetAuthField(string name, string password)
        {
            var FieldLogin = GetLogin();
            FieldLogin.Click();
            FieldLogin.SendKeys(name);
            FieldLogin.SendKeys(Keys.Enter);

            var FieldPassword = GetPassword();
            FieldPassword.Click();
            FieldPassword.SendKeys(password);
            FieldPassword.SendKeys(Keys.Enter);
        }
        
    }
}
