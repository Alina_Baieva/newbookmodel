﻿using OpenQA.Selenium;
using UI_Testing_30._09._21.POM.Locators;
using UI_Testing_30._09._21.Support;

namespace UI_Testing_30._09._21.POM.Methods
{
    internal class RegistrationPage1Methods
    {
        internal static IWebElement GetButtonSignUp() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._buttonSignUp);
        static IWebElement GetFirstName() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._firstName);
        static IWebElement GetLastName() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._lastName);
        static IWebElement GetEmail() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._email);
        static IWebElement GetPassword() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._password);
        static IWebElement GetPasswordCobfirm() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._passwordCobfirm);
        static IWebElement GetPhoneNumber() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._phoneNumber);
        static IWebElement GetCode() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._code);
        internal static IWebElement GetButtonNext() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._buttonNext);
        internal static string GetUrl() => ChromeBrowser.GetDriver().Url;
        //errors:
        internal static string GetErrorFirstName() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._nameError).Text;
        internal static string GetErrorLastName() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._lastNameError).Text;
        internal static string GetErrorMail() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._emailError).Text;
        internal static string GetErrorPassword() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._passwordError).Text;
        internal static string GetErrorConfigPassword() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._configPasswordError).Text;
        internal static string GetErrorPhone() => ChromeBrowser.GetDriver().FindElement(RegistrationPage1Locators._phoneError).Text;

        internal static void Registration(string name, string lastname, string email, string pass, string configPass, string phone)
        {
            var FieldFirstName = GetFirstName();
            FieldFirstName.Click();
            FieldFirstName.SendKeys(name);
            FieldFirstName.SendKeys(Keys.Enter);

            var FieldLastName = GetLastName();
            FieldLastName.Click();
            FieldLastName.SendKeys(lastname);
            FieldLastName.SendKeys(Keys.Enter);

            var FieldMail = GetEmail();
            FieldMail.Click();
            FieldMail.SendKeys(email);
            FieldMail.SendKeys(Keys.Enter);

            var FieldPassword = GetPassword();
            FieldPassword.Click();
            FieldPassword.SendKeys(pass);
            FieldPassword.SendKeys(Keys.Enter);

            if (!configPass.Equals(""))
            {
                var FieldPasswordCobfirm = GetPasswordCobfirm();
                FieldPasswordCobfirm.Click();
                FieldPasswordCobfirm.SendKeys(configPass);
                FieldPasswordCobfirm.SendKeys(Keys.Enter);
            }

            var FieldPhoneNumber = GetPhoneNumber();
            FieldPhoneNumber.Click();
            FieldPhoneNumber.SendKeys(phone);
            FieldPhoneNumber.SendKeys(Keys.Enter);

        }

        public void EnterRefferalCode(string codename)
        {
            var fieldCode = GetCode();
            fieldCode.Click();

            fieldCode.SendKeys(codename);
            fieldCode.SendKeys(Keys.Enter);
        }
    }
}
