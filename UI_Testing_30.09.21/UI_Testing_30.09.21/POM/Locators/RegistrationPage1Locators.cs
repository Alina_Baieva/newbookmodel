﻿using OpenQA.Selenium;

namespace UI_Testing_30._09._21.POM.Locators
{
    internal class RegistrationPage1Locators
    {
        internal static readonly By _buttonSignUp = By.XPath("/html/body/nb-app/nb-home-page/common-react-bridge/div/header/span[1]/button");
        internal static readonly By _firstName = By.Name("first_name");
        internal static readonly By _lastName = By.Name("last_name");
        internal static readonly By _email = By.Name("email");
        internal static readonly By _password = By.Name("password");
        internal static readonly By _passwordCobfirm = By.Name("password_confirm");
        internal static readonly By _phoneNumber = By.Name("phone_number");
        internal static readonly By _buttonNext = By.XPath("/html/body/nb-app/nb-signup/common-react-bridge/div/div[2]/div/section/section/div[1]/form/section/section/div/div[2]/section/div/section/button");
        internal static readonly By _code = By.Name("code");

        internal static readonly By _nameError = By.CssSelector("input[name='first_name']+div[class^='FormErrorText'] > div");
        internal static readonly By _lastNameError = By.CssSelector("input[name='last_name']+div[class^='FormErrorText'] > div");
        internal static readonly By _emailError = By.CssSelector("input[name='email']+div[class^='FormErrorText'] > div");
        internal static readonly By _passwordError = By.CssSelector("input[name='password']+div[class^='FormErrorText'] > div");
        internal static readonly By _configPasswordError = By.CssSelector("input[name='password_confirm']+div[class^='FormErrorText'] > div");
        internal static readonly By _phoneError = By.CssSelector("input[name='phone_number']+div[class^='FormErrorText'] > div");
    }
}
