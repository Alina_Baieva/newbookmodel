﻿using OpenQA.Selenium;

namespace UI_Testing_30._09._21.POM.Locators
{
    internal class RegistrationPage2Locators
    {
        internal static readonly By _companyName = By.CssSelector(" input[name='company_name']");
        internal static readonly By _companySite = By.Name("company_website");
        internal static readonly By _location = By.Name("location");
        internal static readonly By _listLocation = By.Name("_hjRemoteVarsFrame");
        internal static readonly By _selectIndustry = By.XPath("//input[@class='Select__valueBox--2VlHF']");
        internal static readonly By _cosmaticsIndustry = By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[3]");
        internal static readonly By _otherIndustry = By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[7]");
        internal static readonly By _inputIndustry = By.CssSelector("input[name=industry_other]");
        internal static readonly By _buttonFinish = By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/div/button");


        internal static readonly By _companyNameError = By.CssSelector("input[name='company_name']+div[class^='FormErrorText'] > div");
        internal static readonly By _companySiteError = By.CssSelector("input[name='company_website']+div[class^='FormErrorText'] > div");
        internal static readonly By _adressError = By.CssSelector("input[name='location']+div[class^='FormErrorText'] > div");
        internal static readonly By _industryError = By.CssSelector("input[name='industry_other']+div[class^='FormErrorText'] > div");
    }
}
