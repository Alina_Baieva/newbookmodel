﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_Testing_30._09._21.POM.Locators
{
    internal class AuthLocators
    {
        internal static readonly By _LoginButton = By.XPath("/html/body/nb-app/nb-home-page/common-react-bridge/div/header/span[1]/a[2]");
        internal static readonly By _login = By.Name("email");
        internal static readonly By _password = By.Name("password");
        internal static readonly By _buttonLogIn = By.XPath("/html/body/nb-app/ng-component/common-react-bridge/div/div[2]/div/form/section/section/div[3]/button");

        internal static readonly By _loginError = By.CssSelector("input[name='email']+div[class^='FormErrorText'] > div");
        internal static readonly By _passwordError = By.CssSelector("input[name='password']+div[class^='FormErrorText'] > div");
    }
}
