﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_Testing_30._09._21
{
    class GenerateLogin
    {
        const string name0 = "test";
        const string name1 = "nAstya";
        const string name2 = "lifE";
        const string name3 = "TEST";
        const string name4 = "TEST1";
        const string name5 = "Tysya";

        const string name6 = "STASIA";
        const string name7 = "Mihail";
        const string name8 = "30203";
        const string name9 = "Alina";
        const string name10 = "Katya";
        const string name11 = "Tysya";

        public static string MakeLogin()
        {
            string randomLogin = "";
            Random random = new Random();
            var name = random.Next(0, 6);
            switch (name)
            {
                case 0:
                    randomLogin += name0;
                    break;
                case 1:
                    randomLogin += name1;
                    break;
                case 2:
                    randomLogin += name2;
                    break;
                case 3:
                    randomLogin += name3;
                    break;
                case 4:
                    randomLogin += name4;
                    break;
                case 5:
                    randomLogin += name5;
                    break;
            }
            var num = (DateTime.Now.ToBinary() * -1).ToString();
            num = num.Substring(9, 9);

            return randomLogin + num;
        }
        public static string MakeLoginWitn_()
        {
            string randomLogin = "";
            Random random = new Random();
            var name = random.Next(0, 6);
            var randomCount = random.Next(1, 3);

            for (int i = 0; i < randomCount; i++)
            {
                switch (name)
                {
                    case 0:
                        randomLogin += name6 + "_";
                        break;
                    case 1:
                        randomLogin += name7 + "_";
                        break;
                    case 2:
                        randomLogin += name8 + "_";
                        break;
                    case 3:
                        randomLogin += name9 + "_";
                        break;
                    case 4:
                        randomLogin += name10 + "_";
                        break;
                    case 5:
                        randomLogin += name11 + "_";
                        break;
                }
            }
            var num = "";
            if (randomCount == 1)
            {
                num = (DateTime.Now.ToBinary() * -1).ToString();
                num = num.Substring(9, 9);
            }
            return randomLogin + num;
        }
    }
}
